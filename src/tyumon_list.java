import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class tyumon_list {
    private JLabel top;
    private JButton Coffee1Button;
    private JPanel root;
    private JTextPane textPane1;
    private JButton OrangePieButton;
    private JButton StewButton;
    private JButton MilkCakeButton;
    private JButton EspressoButton;
    private JLabel total;
    private JButton cashButton;
    private JButton allDeleteButton;
    private JButton submitButton;
    private JTextPane requestIfYouFinishTextPane;
    private JButton CapreseButton;

    int totalprice=0;



    public tyumon_list() {
        Coffee1Button.setIcon(new ImageIcon(this.getClass().getResource("Coffee.png")));
        OrangePieButton.setIcon(new ImageIcon(this.getClass().getResource("OrangePie.png")));
        StewButton.setIcon(new ImageIcon(this.getClass().getResource("Stew.png")));
        MilkCakeButton.setIcon(new ImageIcon(this.getClass().getResource("MilkCake.png")));
        EspressoButton.setIcon(new ImageIcon(this.getClass().getResource("Espresso.png")));
        CapreseButton.setIcon(new ImageIcon(this.getClass().getResource("Caprese.png")));
        top.setIcon(new ImageIcon(this.getClass().getResource("top.jpeg")));

        textPane1.setText("<Total Order>\n");


        total.setText("Total "+totalprice+"yen");
        Coffee1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee",500);
            }
        });
        OrangePieButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Orange Pie",800);
            }
        });
        StewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Stew",750);
            }
        });
        MilkCakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Milk Cake",700);
            }
        });
        EspressoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Espresso",550);
            }
        });
        root.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);

            }
        });
        allDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel the order?",
                        "Order Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    totalprice = 0;
                    total.setText(totalprice + "yen");
                    String currentText = null;
                    textPane1.setText("<Total Order>\n");
                }
            }
        });
        cashButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to pay?",
                        "Pay Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    if(totalprice==0){
                        JOptionPane.showMessageDialog(null, "You haven't ordered food yet","Order Confirmation",JOptionPane.INFORMATION_MESSAGE);
                    }
                    if(totalprice!=0) {
                        JOptionPane.showMessageDialog(null, "Thank you for using","Order Confirmation",JOptionPane.INFORMATION_MESSAGE);
                        totalprice=0;
                        total.setText(totalprice + "yen");
                        String currentText = null;
                        textPane1.setText("<Total Order>\n");

                    }

                }
            }
        });
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Thank you!!","Request Complete",JOptionPane.INFORMATION_MESSAGE);
                String currentText2 = requestIfYouFinishTextPane.getText();
                currentText2 = null;
                requestIfYouFinishTextPane.setText("Request(if you finish writing,please press the submit button below )\n");



            }
        });
        CapreseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    order("Caprese",650);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("tyumon_list");
        frame.setContentPane(new tyumon_list().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            totalprice+=price;
            total.setText(totalprice + "yen");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "!","Order Complete",JOptionPane.INFORMATION_MESSAGE);
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food +" "+ price + "yen\n");
        }
    }
}
